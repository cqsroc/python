#數字
3456
#浮點數
3.5
#字串  雙引號
"測試中文"
"Hello World"
#布林值
True
False
#有順序、可動的列表 List
[3,4,5]
["Hello","World"]
#有順序、不可動的列表 Tuple
(3,4,5)
("HELLO")
#集合 Set  沒有順序的概念，列表有順序概念
{"HELLO","World"}
#字典 Dictionart
{"apple":"蘋果","data":"資料"}
#變數 用來儲存資料的自訂名稱
#變數名稱=資料
x=3
#print(資料)
print(x)
#x放進其他變數、變數資料將會被取代
x=True
print(x)



